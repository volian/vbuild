vbuild 1 "Sept 1 2024" vbuild "User Manual"
==================================================

# NAME
vbuild - Volian Linux build system

# SYNOPSIS
**vbuild**

# DESCRIPTION
**vbuild** manpage is a work in progress.

# EXAMPLES
Convert the markdown file *vbuild.1.md* into a manpage:
```
vbuild < vbuild.1.md > vbuild.1
```

Same, but using command line arguments instead of shell redirection:
```
vbuild -in=vbuild.1.md -out=vbuild.1
```

# AUTHORS
Blake Lee <https://gitlab.com/volitank> <blake@volian.org>

volian-team <https://gitlab.com/volian> <volian-devel@volian.org>

# COPYRIGHT
Copyright (C) 2024 Blake Lee
