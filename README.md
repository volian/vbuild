# vbuild

The Volian Build System. Work in Progress!

There are no cli options.

vbuild links the source directory `./` to podman using a bind mount.
It generates a script named `./vbuild/init.sh`.
`init.sh` fetches the source if required, and copies it to `./vbuild/build`.
It then runs `debuild` on the copied source.
Afterward, it locates all `debuild` generated files and moves them to `./vbuild/artifacts`.

## Global Config

A global config at `~/.config/volian/vbuild.conf` would look like this:

```toml
# Optional. If missing the project is build unsigned.
key = "<GPG_KEY_ID>"
# Optional. default is `~/.gnupg`
keychain = "~/.gnupg"

```

This config is optional, but required for signing.

## Project Config

In the root of the project source at `vbuild.conf` looks like:

```toml
[git]
# Required
url = "https://gitlab.com/volian/nala.git"
# Required (for now)
tag = "v0.15.1"
```

Neither of these keys are optional, but the config as a whole is.

If there is no config file, the project assumes it's standard Debian packaging.

For now Git and Debian are the only methods implemented. More will come as needed.

Git method still needs the debian files, but does not require the source code to sit with it.

There are no real dependencies other than `podman` as everything happens in the container.
