#!/usr/bin/env just --justfile

# Run checks
check: spellcheck clippy
	@cargo +nightly fmt --check
	@echo Checks were successful!

# Remove generated artifacts
clean:
	rm -f docs/vbuild.1
	rm -rf docs/site
	cargo clean
	rm -rf vbuild/
	@echo Done!

# Build the dockerfile with podman
podman +ARGS="":
	podman login -u $REGISTRY_USER -p $REGISTRY_TOKEN registry.gitlab.com
	cargo build --release
	podman build --tag registry.gitlab.com/volian/vbuild:scar -f ./dockerfile {{ARGS}}
	podman push registry.gitlab.com/volian/vbuild:scar

docs:
	#!/bin/sh

	set -e

	cd docs

	page=vbuild.1
	go-md2man -in="${page}.md" -out=$page

	# Build docs
	doctave build --release

release:
	cargo build --release
	just docs

# Run tests
test +ARGS="":
	@cargo test --no-fail-fast

# Lint the codebase
clippy +ARGS="":
	@cargo clippy \
		--all-targets \
		--all-features \
		--workspace \
		--no-deps \
		-- --deny warnings {{ARGS}}
	@echo Lint successful!

# Format the codebase
fmt +ARGS="":
	@cargo +nightly fmt --all -- {{ARGS}}
	@echo Codebase formatted successfully!

# Spellcheck the codebase
spellcheck +ARGS="--skip target*":
	@codespell --skip="./po"--builtin clear,rare,informal,code --ignore-words-list mut,crate {{ARGS}}
	@echo Spellings look good!
