use std::path::PathBuf;
use std::process::Command;

use anyhow::{Context, Result, anyhow, bail};
use clap::Parser;
use serde::{Deserialize, Serialize};

mod util;

#[derive(Debug, Serialize, Deserialize)]
pub struct Git {
	url: String,
	tag: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum BuildType {
	Git(Git),
	Archive,
}

#[derive(Parser, Debug, Deserialize)]
#[clap(name = "vbuild")]
#[clap(author = "Blake Lee <blake@volian.org>")]
#[clap(version = "0.1.0")]
#[clap(about = "The Volian build system", long_about = None)]
pub struct Builder {
	/// Use Podman for the build
	#[clap(global = true, short, long, action)]
	pub podman: bool,

	/// Do not execute the build script.
	///
	/// With `--podman`, this drops to a shell in the container
	/// after the build script is generated but before it is executed.
	///
	/// This will never execute the build script.
	/// You can run it manually in the container at /src/vbuild/init.sh
	#[clap(global = true, short, long, action)]
	pub no_exec: bool,

	/// The identifier for the gpg key that will be used to sign the source
	/// package.
	#[clap(long, action)]
	pub key_id: Option<String>,

	/// Specify a path to a keychain.
	///
	/// Default: "${HOME}/.gnupg"
	#[clap(short, long, value_parser, value_name = "FILE")]
	pub keychain: Option<PathBuf>,
}

impl Builder {
	pub fn base_path(&self) -> String {
		if let Ok(pwd) = std::env::var("PWD") { pwd } else { ".".to_string() }
	}

	/// Builds the init script based on config
	pub fn init_sh(&self) -> Result<Vec<String>> {
		let mut bootstrap = vec![
			"#!/bin/bash".to_string(),
			"set -e".to_string(),
			"export DEBIAN_FRONTEND=noninteractive".to_string(),
			"apt-get update".to_string(),
		];

		let mut version = String::from_utf8(
			Command::new("dpkg-parsechangelog")
				.args(["--show-field", "version"])
				.output()?
				.stdout,
		)?;

		// Split at Debian revision
		if let Some(split) = version.trim().split_once('-') {
			// Take out the epoch for pkg.tar.orig.xz
			version =
				if split.0.contains(':') { split.0.split_once(':').unwrap().1 } else { split.0 }
					.to_string();
		}

		let (source, mut find_files) =
			util::get_control_info(&util::read_to_string("debian/control")?)?;

		let base_path = if self.podman { "/src".to_string() } else { self.base_path() };
		let build_dir = base_path.clone() + "/vbuild/build";

		if let Ok(config) = util::read_to_string("./vbuild.conf") {
			// If the config exists it needs to be parsable.
			match toml::from_str::<BuildType>(&config)? {
				BuildType::Git(git) => {
					bootstrap.extend([
						format!(
							"git clone --depth 1 --recursive --branch {} {} {build_dir}",
							git.tag, git.url
						),
						format!(
							"tar --create --xz --directory {build_dir} --file \
							 {base_path}/vbuild/{source}_{version}.orig.tar.xz ./"
						),
						format!("rsync --archive {base_path}/debian {build_dir}"),
					]);
				},
				BuildType::Archive => todo!(),
			}
		} else {
			// If there is no config it's considered a Proper Debian build
			// TODO: Need to make tar file when there is a revision.
			bootstrap.push(format!(
				"rsync --archive --exclude vbuild {base_path}/ {build_dir}/"
			));
		}

		bootstrap.extend([
			format!("cd {build_dir}"),
			"eatmydata mk-build-deps --install --remove --tool='apt-get -o \
			 Debug::pkgProblemResolver=yes --yes' debian/control"
				.to_string(),
		]);

		find_files.insert(source);

		let find_command = format!(
			"find {base_path}/vbuild -maxdepth 1 -type f \\( -name '*.dsc' -o -name '*.deb' -o \
			 -name '*.changes' -o -name '*.build*' -o -name '*.tar*' \\) -exec mv {{}} \
			 {base_path}/vbuild/artifacts \\;",
		);

		let debuild: Vec<String> = ["debuild", "--preserve-env", "--preserve-envvar", "PATH"]
			.iter()
			.map(ToString::to_string)
			.collect();

		for switch in ["-S", "-b"] {
			let mut command = debuild.join(" ") + " " + switch;
			if let Some(key) = &self.key_id {
				command += &format!(" --sign-keyid='{key}'");
			}

			bootstrap.push(command);
			bootstrap.push(find_command.clone());
		}

		Ok(bootstrap)
	}

	pub fn exec(&self) -> Result<()> {
		// Eventually consider https://crates.io/crates/podman-api
		let mut cmd = if self.podman {
			let mut cmd = Command::new("podman");
			cmd.args([
				"run",
				"--pull",
				"newer",
				"--rm",
				"--interactive",
				"--tty",
				"--volume",
				"/tmp/vcache:/var/cache/apt/archives",
				"--volume",
				"./:/src",
				"--volume",
			]);

			if let Some(keychain) = &self.keychain {
				cmd.arg(format!("{keychain:?}:/root/.gnupg"));
			} else {
				cmd.arg(format!("{}/.gnupg:/root/.gnupg", std::env::var("HOME")?));
			}

			cmd.arg("registry.gitlab.com/volian/vbuild:scar");
			// Uncomment to drop into the container without running the script
			if !self.no_exec {
				cmd.arg("/src/vbuild/init.sh");
			}
			cmd
		} else {
			Command::new(format!("{}/vbuild/init.sh", self.base_path()))
		};

		if !self.podman && self.no_exec {
			println!("Not executing {cmd:?}");
			return Ok(());
		}

		let status = cmd
			.status()
			.with_context(|| format!("running {:#?}", cmd))?;

		if !status.success() {
			bail!("{cmd:#?} failed with {status}");
		}
		Ok(())
	}
}

fn main() -> Result<()> {
	let builder = Builder::parse();
	let base_path = builder.base_path();

	// Remove old build dir
	runcmd!("rm", "-rf", format!("{base_path}/vbuild"))?;
	// Create build dirs
	util::mkdir(&format!("{base_path}/vbuild/artifacts"))?;
	util::mkdir(&format!("{base_path}/vbuild/build"))?;
	util::mkdir("/tmp/vcache")?;

	let script = format!("{base_path}/vbuild/init.sh");
	let contents = builder.init_sh()?.join("\n");
	println!(
		"
			######################
			# Executing  init.sh #
			######################
		"
	);
	println!("{contents}");
	println!(
		"
			######################
			# End   of   init.sh #
			######################
		"
	);

	// Write the default build script
	util::write_file(&script, &contents)?;
	// Make script executable
	runcmd!("chmod", "+x", script)?;

	builder.exec()?;

	// I don't know why this is documented here but it may be useful
	//
	// In case of bad dput to sid
	// dcut -k <KEY_ID> rm nala*
	//
	// dput ../nala_0.15.1_source.changes
	Ok(())
}

#[cfg(test)]
mod test {
	use super::BuildType;

	#[test]
	fn vbuild_config() {
		let vbuild = toml::from_str::<BuildType>(
			r#"
		[git]
		url = "https://gitlab.com/volian/nala.git"
		tag = "v0.15.1"
	"#,
		)
		.unwrap();

		match vbuild {
			BuildType::Git(git) => {
				assert!(git.url == "https://gitlab.com/volian/nala.git");
				assert!(git.tag == "v0.15.1");
			},
			_ => unimplemented!(),
		}
	}
}
