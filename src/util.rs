use std::collections::HashSet;
use std::path::Path;

use anyhow::{Context, Result, bail};
use rust_apt::tagfile::parse_tagfile;

/// Runs the provided command.
///
/// The first macro argument is the executable, and
/// following arguments are passed to the command. Returns a Result<()>
/// describing whether the command failed. Errors are adequately prefixed with
/// the full command.
#[macro_export]
macro_rules! runcmd {
	($cmd:expr) => (runcmd!($cmd,));
	($cmd:expr, $($args:expr),*) => {{
		let mut cmd = Command::new($cmd);
		$( cmd.arg($args); )*
		let status = cmd.status().with_context(|| format!("running {:#?}", cmd))?;
		if !status.success() {
			Result::Err(anyhow!("{:#?} failed with {}", cmd, status))
		} else {
			Result::Ok(())
		}
	}}
}

pub fn read_to_string<P: AsRef<Path> + std::fmt::Debug + ?Sized>(path: &P) -> Result<String> {
	let ret =
		std::fs::read_to_string(path).with_context(|| format!("Failed to read '{path:?}'"))?;
	println!("Read file '{path:?}'");
	Ok(ret)
}

/// like fs::write but it has added context for failure.
pub fn write_file<P: AsRef<Path> + std::fmt::Debug + ?Sized>(
	path: &P,
	contents: &str,
) -> Result<()> {
	std::fs::write(path, contents).with_context(|| format!("Failed to write '{path:?}'"))?;
	println!("Wrote file '{path:?}'");
	Ok(())
}

// Like fs::create_dir_all but it has added context for failure.
pub fn mkdir<P: AsRef<Path> + std::fmt::Debug + ?Sized>(path: &P) -> Result<()> {
	std::fs::create_dir_all(path).with_context(|| format!("Failed to create '{path:?}'"))?;
	println!("Created file '{path:?}'");
	Ok(())
}

pub fn get_control_info(control: &str) -> Result<(String, HashSet<String>)> {
	let mut source = None;
	let mut find_files = HashSet::new();

	// Control file must exist, for now
	for section in parse_tagfile(control)? {
		if let Some(src) = section.get("Source") {
			source = Some(src.to_string());
			continue;
		}

		if let Some(pkg) = section.get("Package") {
			find_files.insert(pkg.to_string());
		}
	}

	if find_files.is_empty() {
		bail!("No Package Field in debian/control!")
	}

	Ok((
		source.context("No Source Field in debian/control!")?,
		find_files,
	))
}

#[cfg(test)]
mod test {
	use super::get_control_info;

	#[test]
	fn test_control_info() {
		let (source, find_files) = get_control_info(
			"\
Source: src
Priority: optional
Homepage: https://gitlab.com/volian/nala

Package: pkg1
Architecture: all
Description: Commandline frontend for the APT package manager
 Nala is a frontend for the APT package manager. It has a lot
 of the same functionality, but formats the output to be more
 human readable. Also implements a history function to see past
 transactions and undo/redo them. Much like Fedora's dnf history.

Package: pkg2",
		)
		.unwrap();

		assert_eq!(source, "src");
		assert!(find_files.contains("pkg1"));
		assert!(find_files.contains("pkg2"));
	}
}
