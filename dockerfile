FROM debian:sid

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/.cargo/bin
ENV CARGO_HOME=/root/.cargo
ENV RUSTUP_HOME=/root/.rustup

RUN apt-get update \
	&& apt-get install --yes eatmydata

RUN eatmydata apt-get full-upgrade --yes

RUN eatmydata apt-get install --yes \
	apt-utils \
	build-essential \
	devscripts \
	rsync \
	just \
	codespell \
	libapt-pkg-dev \
	podman \
	&& apt-get clean

RUN curl https://sh.rustup.rs -sSf | sh -- /dev/stdin -y
RUN rustup toolchain install nightly
RUN rustup default stable
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN cargo install --git https://github.com/Doctave/doctave --tag 0.4.2

ADD target/release/vbuild /usr/bin/vbuild

RUN echo "deb-src http://deb.debian.org/debian/ sid main" >> /etc/apt/sources.list
